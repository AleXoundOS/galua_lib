{ stdenv, pkgs }:
stdenv.mkDerivation {
  name = "galua_lib";

  src = builtins.path {path = ./.;};

  outputs = [ "out" "dev" ];

  buildPhase = ''
    gcc -c -Wall -Werror -fpic -o galua.o sources/galua.c
    gcc -shared -o galua_lib.so galua.o
  '';

  installPhase = ''
    mkdir -p $dev/include
    cp headers/galua.h $dev/include

    mkdir -p $out/bin
    cp galua_lib.so $out/bin
  '';
}
